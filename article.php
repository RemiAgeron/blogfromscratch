<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
	<title>Blog From Scratch - Acceuil</title>
	<link rel="stylesheet" href="style.css">
</head>

<body>
	
  <header>
    <div class="header_logo">B</div>
    <h1>Blog from scratch</h1>
  </header>

  <a class="article_back" href="index.php">< Back</a>

  <?php include 'connection.php';

  if(!empty($_POST)) {
    $id = (int)$_POST['id'];
    $req_check_id = $db->query("SELECT id FROM articles WHERE id = '$id'");
  } else {
    echo "Sorry you've reached the end of the known world";
    die;
  }

  if(isset($req_check_id)) {

    $req = ($db->query("SELECT * FROM articles 
    JOIN authors USING(author_id)
    WHERE id = $id"))->fetch_array();

    $req_cat = $db->query("SELECT category FROM categories
    JOIN articles_categories ON articles_categories.category_id = categories.id
    WHERE article_id = '$id'");

  } else {
    echo "Sorry you've reached the end of the known world";
    die;
  }
  ?>

  <h2 class="article_title"><?php echo $req['title'] ?></h2>

  <div class="article_content"><?php echo $req['content'] ?></div>

  <img class="article_image" src="<?php echo $req['image_url'] ?>" alt="<?php echo $req['image_alt '] ?>">

  <div class="article_footer">

    <div class="article_author">
      <h5>By : </h5>
      <form action="index.php" method="post">
        <input type="hidden" name="aut" value="<?php echo $req['firstname'] . " " . $req['lastname'] ?>">
        <input class="form_btn" type="submit" value="<?php echo $req['firstname'] . " " . $req['lastname'] ?>">
      </form>
    </div>

    <div class="list_article_category">
      
      <?php if(mysqli_num_rows($req_cat)) {
        
        foreach($req_cat as $category) { ?>
          <form action="index.php" method="post">
            <input type="hidden" name="cat" value="<?php echo $category['category'] ?>">
            <input class="form_btn article_category" type="submit" value="<?php echo $category['category'] . "  " ?>">
          </form>

        <?php }
      } ?>

    </div>

  </div>

  <footer>2021 - blogfromscratch - Rémi</footer>
</body>

</html>