<!DOCTYPE html>
<html lang="fr">

<head>
  <meta charset="UTF-8">
	<title>Blog From Scratch - Acceuil</title>
	<link rel="stylesheet" href="style.css">
</head>

<body>

<?php include 'connection.php';

if(!empty($_POST)) {

  if(isset($_POST['aut'])) {

    $aut = htmlspecialchars($_POST['aut']);
    $req_check = ($db->query("SELECT author_id FROM authors 
      WHERE concat(firstname,' ',lastname) = '$aut'"))->fetch_array();

    $aut_id = $req_check['author_id'];
    $req_post = "SELECT * FROM articles 
      JOIN authors USING(author_id) 
      WHERE author_id = '$aut_id'
      ORDER BY published_at DESC";

    $filter = $aut;

  }

  if(isset($_POST['cat'])) {
    
    $cat = htmlspecialchars($_POST['cat']);
    $req_check = ($db->query("SELECT id FROM categories 
      WHERE category = '$cat'"))->fetch_array();

    $cat_id = $req_check['id'];
    $req_post = "SELECT * FROM articles 
      JOIN authors USING(author_id) 
      JOIN articles_categories ON articles.id = articles_categories.article_id
      WHERE category_id = '$cat_id'
      ORDER BY published_at DESC";

    $filter = $cat;

  }

  if(isset($req_check)) {
    $req_art = $db->query($req_post);

  } else {
    echo "Sorry you've reached the end of the known world";
    die;
  }
  
} else {

  $req_art = $db->query("SELECT * FROM articles 
    JOIN authors USING(author_id) 
    ORDER BY published_at DESC");

}

$req_all_cat = $db->query("SELECT category FROM categories 
  ORDER BY category");

$req_all_aut = $db->query("SELECT firstname, lastname, email FROM authors 
  ORDER BY firstname"); ?>

	<header>
    <div class="header_logo">B</div>
    <h1>Blog from scratch</h1>
  </header>

  <div class="list_dropdown">
    <div class="flex"></div>
    <?php if(isset($filter)) { ?>
      <div class="dropdown_nofilter">
        <div class="dropdown_filter_tag"><?php echo $filter ?></div>
        <a href="index.php">x</a>
      </div>
      <?php } ?>
    <div class="dropdown">
      <button class="dropdown_head">Filter by author</button>
      <div class="dropdown_list">
        <?php foreach($req_all_aut as $author) { ?>
          <form class="dropdown_link" action="index.php" method="post">
            <input type="hidden" name="aut" value="<?php echo $author['firstname'] . " " . $author['lastname'] ?>">
            <input class="form_btn" type="submit" value="<?php echo $author['firstname'] . " " . $author['lastname'] ?>">
          </form>
        <?php } ?>
      </div>
    </div>
    <div class="dropdown">
      <button class="dropdown_head">Filter by category</button>
      <div class="dropdown_list">
        <?php foreach($req_all_cat as $category) { ?>
          <form class="dropdown_link" action="index.php" method="post">
            <input type="hidden" name="cat" value="<?php echo $category['category'] ?>">
            <input class="form_btn" type="submit" value="<?php echo $category['category'] ?>">
          </form>
        <?php } ?>
      </div>
    </div>
  </div>

  <div class="grid">

    <?php foreach($req_art as $article) { ?>

      <div class="preview">

        <h3 class="article_title"><?php echo $article['title'] ?></h3>

        <div class="head">
          <div>by : <?php echo $article['firstname'], " ", $article['lastname'] ?></div>
          <div class="flex"></div>
          <div>at : <?php echo $article['published_at'] ?></div>
        </div>

        <div class="preview_content"><?php echo $article['content'] ?></div>

        <form class="article_link" action="article.php" method="post">
            <input type="hidden" name="id" value="<?php echo $article['id'] ?>">
            <input class="form_btn" type="submit" value="Read More">
        </form>

        <?php $article_id = $article['id'];

        $req_art_cat = $db->query("SELECT category FROM categories
        JOIN articles_categories ON articles_categories.category_id = categories.id
        WHERE article_id = '$article_id'");

        if(mysqli_num_rows($req_art_cat)) { ?>
          <div class="preview_bottom">Category : <?php
            foreach($req_art_cat as $category) { ?> 
              <form action="index.php" method="post">
                <input type="hidden" name="cat" value="<?php echo $category['category'] ?>">
                <input class="form_btn article_category" type="submit" value="<?php echo $category['category'] . "  " ?>">
              </form> 
            <?php
            }
          ?> </div> <?php
        } ?>

      </div>

      <?php
    } ?>

  </div>

  <footer>2021 - blogfromscratch - Rémi</footer>

</body>

</html>